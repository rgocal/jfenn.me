.PHONY: all build serve clean

NPM = npm
NPX = npx

DESTDIR?=out

GITHUB_TOKEN = $(shell pass github.com/fennifith/token)

all: serve

install: package-install.lock

package-install.lock: package.json
	${NPM} install
	touch package-install.lock

build: install
	GITHUB_TOKEN=${GITHUB_TOKEN} ${NPX} @11ty/eleventy
	GITHUB_TOKEN=${GITHUB_TOKEN} ELEVENTY_ENV=production ${NPX} @11ty/eleventy

serve: install
	GITHUB_TOKEN=${GITHUB_TOKEN} ${NPX} @11ty/eleventy --serve

# TODO: replace this with better CI
deploy: prune build
	touch _site/.nojekyll
	git add -f _site/
	git commit -m 'deploy'
	git subtree split --prefix _site -b temp
	git push -f deploy temp:main
	git branch -D temp
	git reset HEAD~

prune:
	rm -rf _site/
	rm -rf .node-persist/

clean: prune
	rm -f package-install.lock
	rm -f pnpm-lock.yaml
	rm -rf node_modules/
