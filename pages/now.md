---
layout: base
permalink: /now/
title: Now
---

<div style="height: 300px; background-image: url('/images/headers/snowytrees.webp'); background-size: cover; background-position: center bottom; background-repeat: no-repeat;"></div>
<div class="container text">

This is a [now page](https://nownownow.com/about). It's a simple way to share everything currently happening in my life. I last updated it on 2023-04-15.

## What am I doing now?

- [making strange noises on the internet](https://fennifith.bandcamp.com)
- building weird software for fun; and [streaming it on Twitch](https://www.twitch.tv/fennifith)
- [playing Minecraft](https://minecraft.horrific.dev/) with a group of friends and pondering [confusing redstone mechanics](https://minecraft.gamepedia.com/Tutorials/Quasi-connectivity)
- sipping chai tea and listening to [65daysofstatic](https://65daysofstatic.bandcamp.com/music)

### Groups I'm involved with

[Unicorn Utterances](https://unicorn-utterances.com/) is a collaborative blog site that provides helpful resources for a variety of computer science-related topics.

[Horrific.Dev](https://horrific.dev/) is a small collective of individual developers and enthusiasts with a focus on independent services, software, and technology.

</div>
