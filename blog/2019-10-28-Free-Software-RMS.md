---
layout: layouts/blog
title: "Free Software and RMS"
description: "A summary of my thoughts on the relationship between Richard Stallman and the Free Software Movement."
date: "2019-10-28"
links:
  - name: "1x.engineer"
    url: https://1x.engineer/
  - name: "Remove Richard Stallman"
    url: https://medium.com/@selamie/remove-richard-stallman-fec6ec210794
  - name: "Richard Stallman and the Fall of the Clueless Nerd"
    url: https://www.wired.com/story/richard-stallman-and-the-fall-of-the-clueless-nerd/
  - name: "The Future of the Intersection of PoC and F/LOSS"
    url: https://v2.jacky.wtf/post/intersection-poc-floss
tags: blog
---

Recently, there has been a bit of an uptick in debate over racial and
misogynistic issues in the GNU Project and Free Software Foundation (FSF). This
has been partly spurred by Richard Stallman's involvement in discussion about
[Marvin Minsky's relationship with Jeffery Epstein](https://www.theverge.com/2019/8/9/20798900/marvin-minsky-jeffrey-epstein-sex-trafficking-island-court-records-unsealed).

(I'm not going to discuss Stallman's actions or how they were harmful. That
would bring up some risky topics that I don't want to cover here, keeping in
mind the audience that might be reading my posts. If you want evidence of this,
you can
[easily](https://www.wired.com/story/richard-stallman-and-the-fall-of-the-clueless-nerd/)
[find](https://sfconservancy.org/news/2019/sep/16/rms-does-not-speak-for-us/)
[it](https://arstechnica.com/tech-policy/2019/09/richard-stallman-leaves-mit-after-controversial-remarks-on-rape/)
[elsewhere](https://medium.com/@selamie/remove-richard-stallman-fec6ec210794).)

RMS has since resigned from his positions at MIT CSAIL and the Free Software
Foundation; by now, all this is old news. However, I still wanted to upload this
post because it covers issues that I believe can apply more broadly to the rest
of the community. Similar problems are present in much of the computer science
industry, and it would be a mistake to ignore them.

I've had an idea of what "free software" is for quite a long time. I've seen it
as a way to open up software - to make it available to people that might otherwise
face the control of profit-biased companies. I saw it as a way to make my harmless
little projects a step more inclusive, so that anyone - regardless of social
prejudice - could benefit from them, become involved with their development, and
gain experience necessary to support themselves in this world. With this assumption,
I had a great amount of respect for the creators of the free software movement
because I thought that they shared the same values in creating an open community
for all.

I feel that these are important standards to set, as this is the same way that I
first started in the industry; the ability to hack software together, the
freedom to see how other work is built, and the privilege to interact with a
community of programmers doing the same thing. This community is something that
I want everyone to feel happy to be a part of.

This is not a place for the opinions of Richard Stallman to influence. It's not
something that should be available for anyone to exploit in promoting oppressive
views and excluding the marginalized members of the community. I don't want to
see the community's work being taken advantage of in order to spread hate crime
or violate our fundamental human rights.

To be clear, I've known about Stallman's opinions for a long time. I've read his
political notes and fallen down the rabbit hole of his website on more than a
few occasions. I didn't write about this publicly because I never felt that it
was relevant to what I was doing, but upon reflection I realize that I was
wrong. This is bigger than me and more important than my silly projects or
childish ideals. If you fail to assert a view against something, then you
effectively allow it to spread, and that isn't something that I'm willing to do
here. I am grateful to everyone that wrote about this problem and their
experiences for spreading awareness and forcing me to think about its effect.
This isn't something that I talk about lightly, and I'm open to criticism of
anything that I discuss here.

On to the point...

## Free software isn't everything.

It's important, sure. People need to have trust in the software they use, and
the FSF has gone a long way towards setting a standard for software to
facilitate that, but it isn't everything. I would prefer for all of my software
to be proprietary, depending on closed-source frameworks and compilers with
masses of privacy issues and no transparency whatsoever, than to be a part of
the group that is defending the practices of people like Epstein.

The free software movement is an important philosophy that I still support, and
I want to see it grow. But it isn't worth more than people. It isn't worth the
price of discrimination and oppression of women and minorities that supporting
RMS will inevitably cause. My genuine hope is that free software is more than
this. I hope that it can overcome the influence of the bigotry at its core and
continue to pursue its more honorable intentions, and although I have my doubts,
I will continue to support that idea - because it's not something that will go
away easily. It isn't something that makes sense to destroy; it's something that
needs to be changed.

> The problems are _so obvious_.
>
> There is nothing wrong with women. There is nothing wrong
> with girls in STEM. There are many women and many girls who,
> in spite of everything, love STEM-related disciplines.
> [Those disciplines] are filled to the brim with so, so many
> _shitty men_.
>
> \- Selam Jie Gano, ["Remove Richard Stallman (and everyone else horrible in tech)"](https://medium.com/@selamie/remove-richard-stallman-fec6ec210794)

The problem is that, by using his work, the FOSS community is implicitly
furthering the influence of its creator. Stallman can say, for example, "Look
at me! All of these people depend on me. Women are inferior!" - and that is
immediately representing everyone that supports the free software movement.

Stallman didn't just create free software or contribute to it; he himself
_became_ it. He was idolized as creator of everything good in software, which he
in part propagated by forming [the Church of Emacs](https://stallman.org/saint.html)
and declaring himself a saint.

> Why, exactly, was he allowed to behave this way? Because
> people in this scene have been enabling his behavior on
> the basis of "genius" for decades, and he's never learned
> anything else.
>
> \- Jillian C. York, [twitter.com](https://twitter.com/jilliancyork/status/1173898051586379778)

RMS might be important. His contributions to the free software movement were
indeed significant, and he has formed a significant following of people that
agree with his ideals. However, being technically adept is not an excuse to be a
terrible person. The way that RMS acted - and the fact that others tolerated him
for so long - likely drove many others away from the community, and that isn't
something that I want to support.

## Free software isn't RMS.

The purpose of free software is to permit anti-discriminatory distribution by
definition. It was created to shift important software away from biased
influence into a community that _should_ be more inclusive to people who are
less represented in society. It's designed to give other people a say in the
software they use and depend on. The free / open source movement is one that I
consider to inherently support individuals, but it takes away from that goal
when an organization like the FSF has failed to advocate for the marginalized
side of the community for such a long time.

> We got amazing people laying out how the visible vanguards
> of movements are actively harming people in the space (if
> not directly then by endorsement and support of those who
> do - it's called being an accomplice). I'm thinking that
> it's nigh time that we actually give up on things of the
> GNU. Our endorsement and support is going to further give
> them a head count that, in this space, is implicit power.
> And I refuse to give them that knowing that this behavior
> has been going on for more than a decade.
>
> \- Jacky Alciné, ["The Future of the Intersection of PoC and F/LOSS"](https://v2.jacky.wtf/post/intersection-poc-floss)

At this point it would be a little hypocritical to base my ideals on Stallman's
work. How can I criticize anything that is happening here when I'm
basing all of my ethical ideals on work that RMS did for free? It may be
substantially less significant, but it bears justification from the community as
a whole that accepts his work as their own. The conclusion that I've come to is
that an amount of hypocrisy here is inevitable. We can reasonably minimize that,
but at the end of the day, his work _is_ free software. There's no way to
differentiate between the ideas of free software that he had and the idea of
free software that the community has today - because, on the surface, they're
mostly the same thing.

That said, ideas are bigger than people. Linking an idea to a single person is a
point of failure. People fail; RMS failed - but I don't think that RMS is the
embodiment of free software. Free software isn't defined by him, nor is it
defined by the FSF. It is fundamentally an idea of the community, and one that
can be redefined by it as well.

## Conclusion

<!-- I've had to redo this conclusion a few times, as it was initially quite
disorganized and failed to express the points that I wanted to make. Hopefully
the post makes a bit more sense now that I've simplified & reworded it. -->

In any professional pursuit, the product of your work often isn't the most
important thing to be worried about. Success is not an objective measurement,
and just improving code doesn't necessarily make the result any better. As we
continue to build software, it is important to take the effect that it has on
different people and communities into consideration, both as a project and an
organization. Blindly creating tools with no regard for the communities that
form around them can do as much harm as good, and any individual who perpetuates
that harm cannot be defended solely by their technical expertise.

Please don't gatekeep the community with a developer celebrity that doesn't
deserve their influence. Implement a [reasonable code of conduct](https://www.contributor-covenant.org)
to protect against the gaslighting, bigotry, and harassment that is far too
common in online spaces like these. Strive to be more inclusive to people that
aren't able to represent themselves as freely, and maybe you'll realize how
naive and self-serving the [defensive claims](https://selamjie.medium.com/remove-richard-stallman-appendix-a-a7e41e784f88)
(by Stallman's supporters) of hypercriticism and "cancel culture" really are.
