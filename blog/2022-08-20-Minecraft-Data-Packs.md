---
layout: layouts/redirect
title: "Minecraft Data Pack Programming: Scoreboard Usage"
date: "2022-08-20"
description: "A beginner-friendly series for teaching programming concepts with Minecraft data packs."
redirect: https://unicorn-utterances.com/posts/minecraft-data-packs-scoreboards
link: https://unicorn-utterances.com/posts/minecraft-data-packs-scoreboards
tags: blog
---
