---
layout: layouts/redirect
title: "Minecraft Data Pack Programming: Command Syntax"
date: "2022-06-15"
description: "A beginner-friendly series for teaching programming concepts with Minecraft data packs."
redirect: https://unicorn-utterances.com/posts/minecraft-data-packs-cmd-syntax
link: https://unicorn-utterances.com/posts/minecraft-data-packs-cmd-syntax
tags: blog
---
