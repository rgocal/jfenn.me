const _fs = require("fs");
const _markdown = require("markdown-it");
const _yaml = require("js-yaml");
const _moment = require("moment");
const _postcss = require("postcss");
const _postcssImportUrl = require("postcss-import-url");
const _autoprefixer = require("autoprefixer");
const _cssnano = require("cssnano");
const _csso = require("postcss-csso");
const _purgecss = require("@fullhuman/postcss-purgecss");

const _pluginRss = require("@11ty/eleventy-plugin-rss");
const _pluginTOC = require("eleventy-plugin-nesting-toc");
const _pluginSASS = require("eleventy-sass");
// const _upgradeHelp = require("@11ty/eleventy-upgrade-help");

module.exports = function(eleventyConfig) {
	const isBuild = process.env.ELEVENTY_RUN_MODE === "build";
	eleventyConfig.setDataDeepMerge(true);

	eleventyConfig.addLayoutAlias("default", "layouts/default.liquid");
	eleventyConfig.addLayoutAlias("base", "layouts/base.liquid");
	eleventyConfig.addLayoutAlias("page", "layouts/page.liquid");
	eleventyConfig.addLayoutAlias("redirect", "layouts/redirect.liquid");

	eleventyConfig.addPassthroughCopy("favicon.ico");
	eleventyConfig.addPassthroughCopy("resume.pdf");
	eleventyConfig.addPassthroughCopy({"assets/images": "images"});
	eleventyConfig.addPassthroughCopy({"assets/files": "files"});
	eleventyConfig.addPassthroughCopy({"assets/css": "css"});
	eleventyConfig.addPassthroughCopy({"assets/js": "js"});

	const markdownItRenderer = new _markdown({ html: true })
		.use(require('markdown-it-prism'), { plugins: ["autoloader"] })
		.use(require("markdown-it-anchor"));

	eleventyConfig.addFilter("markdownify", function(str) {
		if (typeof str === 'string')
			return markdownItRenderer.render(str);
	});

	eleventyConfig.setLibrary("md", markdownItRenderer);

	eleventyConfig.addFilter("where", function(array, prop, value) {
		return (array || []).filter((item) => item[prop] == value);
	});

	eleventyConfig.addFilter("dateUTC", function(date, format) {
		return _moment.utc(date).format(format);
	});

	eleventyConfig.addFilter("starts_with", (string, test) => (string + "").startsWith(test));

	eleventyConfig.addDataExtension("yaml", contents => _yaml.safeLoad(contents));

	eleventyConfig.addPlugin(_pluginRss);

	eleventyConfig.addPlugin(_pluginTOC, {
		tags: ['h2', 'h3'],
	});

	eleventyConfig.addPlugin(_pluginSASS, {
		postcss: _postcss([
			_postcssImportUrl({ modernBrowser: true }),
			_purgecss({
				content: ['./_site/**/*.html'],
				safelist: {
					greedy: [/--theme-(light|dark)$/]
				},
			}),
			_autoprefixer,
			_csso,
		]),
		compileOptions: {
			permalink: function(contents, inputPath) {
				return (data) => data.page.filePathStem.replace(/^\/assets\//, "/") + ".css";
			},
		},
	});

	// eleventyConfig.addPlugin(_upgradeHelp);

	eleventyConfig.setLiquidOptions({
		dynamicPartials: true,
		strictFilters: false,
	});

	return {
		templateFormats: [
			"md",
			"njk",
			"html",
			"liquid"
		],

		// If your site lives in a different subdirectory, change this.
		// Leading or trailing slashes are all normalized away, so don’t worry about it.
		// If you don’t have a subdirectory, use "" or "/" (they do the same thing)
		// This is only used for URLs (it does not affect your file structure)
		pathPrefix: "/",

		markdownTemplateEngine: "liquid",
		htmlTemplateEngine: "liquid",
		dataTemplateEngine: "liquid",
		passthroughFileCopy: true,
		dir: {
			input: ".",
			includes: "_includes",
			data: "_data",
			output: "_site"
		}
	};
};
